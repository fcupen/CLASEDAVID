import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../servicio.service';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.css']
})
export class Comp1Component implements OnInit {

  constructor(private ServicioService:ServicioService) { }
  user:string;
  editUser:string; 

  ngOnInit() {
    this.ServicioService.cast.subscribe(user=> this.user = user);
  }

  editTheUser(){
    this.ServicioService.editUser(this.editUser);
  }

}
