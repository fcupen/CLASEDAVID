import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../servicio.service';

@Component({
  selector: 'app-comp2',
  templateUrl: './comp2.component.html',
  styleUrls: ['./comp2.component.css']
})
export class Comp2Component implements OnInit {

  constructor(private ServicioService:ServicioService) { }
  user:string; 

  ngOnInit() {
    this.ServicioService.cast.subscribe(user=> this.user = user);
  } 
}
